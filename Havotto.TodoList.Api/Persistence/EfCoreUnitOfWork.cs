﻿using Havotto.TodoList.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havotto.TodoList.Persistence
{
    public class EfCoreUnitOfWork : IUnitOfWork
    {
        private readonly TodoDbContext _dbContext;

        public EfCoreUnitOfWork(TodoDbContext dbContext)
        {
            this._dbContext = dbContext;
        }
        public Task SaveChangesAsync()
        {
            return _dbContext.SaveChangesAsync();
        }
    }
}
