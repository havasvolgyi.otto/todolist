﻿using Havotto.TodoList.Api.Domain.Repositories;
using Havotto.TodoList.Domain;
using Havotto.TodoList.Domain.Entities;
using Havotto.TodoList.Persistence;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Havotto.TodoList.Api.Persistence.Repositories
{
    public class TodoRepository : ITodoRepository
    {
        //ha azt akarjuk, hogy EF Core könnyebben lecserélhető legyen,
        //akkor itt egy absztrakciót kell használnunk helyette,
        //pl. EfCoreUnitOfWork-öt
        private readonly TodoDbContext _db;

        public TodoRepository(TodoDbContext db)
        {
            this._db = db;
        }
        public TodoItem Create(TodoItem item)
        {
            _db.Add(item);
            return item;
        }

        public async Task<bool> Delete(int id)
        {
            var item = await _db.FindAsync<TodoItem>(id);
            if (item == null)
                return false;
            _db.Remove(item);
            return true;
        }

        /// <summary>
        /// Nullal tér vissza ha nem talál
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<TodoItem> FindByIdAsync(int id)
        {
            return _db.FindAsync<TodoItem>(id).AsTask();
        }

        public Task<List<TodoItem>> List(List<Expression<Func<TodoItem, bool>>> filters, int? pageIndex)
        {
            IQueryable<TodoItem> q = _db.Todos;
            foreach (var filter in filters)
            {
                q = q.Where(filter);
            }
            if (pageIndex is int page && page > 0)
            {
                q = q.Skip(Constants.PageSize * (page - 1))
                    .Take(Constants.PageSize);
            }

            return q.ToListAsync();

        }
    }
}
