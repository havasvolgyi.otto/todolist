﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havotto.TodoList.Persistence
{
    public static class Migrator
    {
        public static void MigrateDatabase(IServiceProvider sp)
        {
            using (var scope = sp.CreateScope())
            {
                var scopedSP = scope.ServiceProvider;
                var db = scopedSP.GetRequiredService<TodoDbContext>();
                db.Database.Migrate();
            }
        }
    }
}

