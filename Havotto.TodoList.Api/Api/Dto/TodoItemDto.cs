﻿using Havotto.TodoList.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havotto.TodoList.Api.Dto
{
    public class TodoItemDto
    {
        public TodoItemDto()
        {
        }

        public TodoItemDto(TodoItem item)
        {
            //ha sok leképzés van, akkor AutoMappert célszerű inkább használni
            Id = item.Id;
            Title = item.Title;
            IsCompleted = item.IsCompleted;
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsCompleted { get; set; }
    }
}
