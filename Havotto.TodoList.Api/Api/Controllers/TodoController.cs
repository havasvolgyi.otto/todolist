﻿using Havotto.TodoList.Api.Dto;
using Havotto.TodoList.Domain.Commands;
using Havotto.TodoList.Domain.Helpers;
using Havotto.TodoList.Domain.Queries;
using Havotto.TodoList.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havotto.TodoList.Api.Controllers
{
    [ApiController]
    [Route("api/todo")]
    public class TodoController : ControllerBase
    {
        private readonly ILogger<TodoController> _logger;
        private readonly ITodoService _todoService;

        public TodoController(ILogger<TodoController> logger,
            ITodoService todoService)
        {
            _logger = logger;
            _todoService = todoService;
        }

        [HttpGet]
        public async Task<List<TodoItemDto>> List(
            [FromQuery] int? isCompleted,
            [FromQuery] string titleContains,
            [FromQuery] int? pageIndex)
        {
            var query = new ListQuery { 
                IsCompleted = isCompleted.ToNullableBool(),
                TitleContains = titleContains,
                PageIndex = pageIndex,
            };

            var items = await _todoService.List(query);
            //a DTO-vá alakítást lehet, hogy célszerűbb lenne
            //beljebb elvégezni, hogy ne keletkezzen két List<T>
            return items.Select(x => new TodoItemDto(x)).ToList();
        }


        [HttpPost]
        public async Task<TodoItemDto> Create(CreateTodoCommand cmd)
        {
            var todo = await _todoService.Create(cmd);
            return new TodoItemDto(todo);
        }

        [HttpPost("{id:int}/mark-completed")]
        public async Task<TodoItemDto> MarkComleted(int id)
        {
            var todo = await _todoService.MarkCompleted(id);
            return new TodoItemDto(todo);
        }

        [HttpPost("{id:int}/change-title")]
        public async Task<TodoItemDto> ChangeTitle(int id, ChangeTodoTitleCommand cmd)
        {
            var todo = await _todoService.ChangeTitle(id, cmd.Title);
            return new TodoItemDto(todo);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _todoService.Delete(id);
            return NoContent();
        }
    }
}
