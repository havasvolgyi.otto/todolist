using Havotto.TodoList.Api.Domain.Repositories;
using Havotto.TodoList.Api.Persistence.Repositories;
using Havotto.TodoList.Domain;
using Havotto.TodoList.Domain.Services;
using Havotto.TodoList.Persistence;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Havotto.TodoList.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSwaggerDocument(o =>
            {
                o.PostProcess = document =>
                {
                    var info = document.Info;
                    info.Title = "Todo API";
                    info.Version = "1.0";
                    info.Description = "Havasvölgyi OttóTodo API";
                };
            });

            services.AddTransient<ITodoService, TodoService>();

            services.AddTransient<ITodoRepository, TodoRepository>();

            AddDbContext(services);

            services.AddScoped<IUnitOfWork, EfCoreUnitOfWork>();
        }

        protected virtual void AddDbContext(IServiceCollection services)
        {
            services.AddDbContext<TodoDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("Todo"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseOpenApi(s => { });
                app.UseSwaggerUi3(s => { s.DocumentTitle = "Todo API"; });
            }

            app.UseErrorHandler();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

