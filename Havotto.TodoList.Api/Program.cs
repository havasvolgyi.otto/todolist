using Havotto.TodoList.Persistence;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Havotto.TodoList.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //TODO serilog logger m�r a legelej�n

            var host = CreateHostBuilder(args).Build();

            //ez �gy kicsiban j� m�dszer
            Migrator.MigrateDatabase(host.Services);

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
