﻿using Havotto.TodoList.Domain.Exceptions;
using Havotto.TodoList.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text.Json;
using System.Threading.Tasks;

namespace Havotto.TodoList.Infrastructure
{
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlerMiddleware> _logger;
        private readonly IWebHostEnvironment _env;

        public ErrorHandlerMiddleware(RequestDelegate next, 
            ILogger<ErrorHandlerMiddleware> logger,
            IWebHostEnvironment env)
        {
            this._next = next;
            this._logger = logger;
            this._env = env;
            
        }
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (EntityNotFoundException ex)
            {
                var response = context.Response;
                response.StatusCode = StatusCodes.Status404NotFound;
                response.ContentType = MediaTypeNames.Application.Json;

                var responseModel = new ProblemDetails()
                {
                    Status = response.StatusCode,
                    Title = $"{ex.EntityType.Name} #{ex.Id} not found",
                };

                var result = JsonSerializer.Serialize(responseModel);
                await response.WriteAsync(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                var response = context.Response;
                response.StatusCode = StatusCodes.Status500InternalServerError;
                response.ContentType = MediaTypeNames.Application.Json;

                var responseModel = new ProblemDetails()
                {
                    Status = response.StatusCode,
                    Title = "Internal server error",
                };

                //fejlesztéskor részletesebb információk
                if (_env.IsDevelopment())
                    responseModel.Detail = ex.ToString();

                var result = JsonSerializer.Serialize(responseModel);
                await response.WriteAsync(result);
            }
        }
    }
}

namespace Microsoft.AspNetCore.Builder
{
    public static class ErrorHandlerMiddlewareApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseErrorHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ErrorHandlerMiddleware>();
        }
    }
}