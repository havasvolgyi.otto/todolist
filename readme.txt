﻿Szükséges eszközök

.NET Core 3.1 SDK

SQL Server localdb

Kipróbálás

1. Futassuk az alkalmazást a Havotto.TodoList.Api mappában: dotnet run
Ez létrehozza a havotto_todo adatbázist a localdb-ban, és migrálja is.

2. Böngészőben töltsük be a következő oldalt: http://localhost:5000/swagger/index.html#/Todo 

Funkciók

-Globális hibakezelés hibanaplózással
-Todo létrehozás címmel, max hossz 200
-Felvett Todo elemek listázása
-Felvett Todo elem készre jelentése
-Kész/nem kész Todo emelek listázása
-Todo törlés
-Todo cím módosítás
-Todo elem szövegre keresés
-Laponként listázás
-A szűrő feltételek együtt is alkalmazhatók akár

Integrációs tesztek

SQLite-ot használok integárciós teszteknél egyelőre
