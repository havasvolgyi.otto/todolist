﻿using Havotto.TodoList.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Havotto.TodoList.Domain.Commands
{
    public class ChangeTodoTitleCommand
    {
        [Required, MaxLength(TodoItem.TitleMaxLength)]
        public string Title { get; set; }
    }
}
