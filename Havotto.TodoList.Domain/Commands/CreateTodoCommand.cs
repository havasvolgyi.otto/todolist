﻿using Havotto.TodoList.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace Havotto.TodoList.Domain.Commands
{
    public class CreateTodoCommand
    {
        //TODO ezt a validációt beljebb kellene kezelni, mert
        //így sok lesz a duplázódás
        [Required, MaxLength(TodoItem.TitleMaxLength)]
        public string Title { get; set; }
    }
}
