﻿using Havotto.TodoList.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Havotto.TodoList.Api.Domain.Repositories
{
    public interface ITodoRepository
    {
        TodoItem Create(TodoItem item);
        Task<TodoItem> FindByIdAsync(int id);
        Task<List<TodoItem>> List(List<Expression<Func<TodoItem, bool>>> filters, int? pageIndex);
        Task<bool> Delete(int id);
    }
}
