﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havotto.TodoList.Domain
{
    public static class Constants
    {
        /// <summary>
        /// Lap mérete lapozó listázáskor
        /// </summary>
        public const int PageSize = 25;
    }
}
