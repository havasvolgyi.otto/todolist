﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havotto.TodoList.Domain.Entities
{
    public class TodoItem
    {
        public int Id { get; set; }

        public const int TitleMaxLength = 200;
        public string Title { get; set; }
        public bool IsCompleted { get; set; }
    }
}
