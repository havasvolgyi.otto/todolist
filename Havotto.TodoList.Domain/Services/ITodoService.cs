﻿using Havotto.TodoList.Domain.Commands;
using Havotto.TodoList.Domain.Entities;
using Havotto.TodoList.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Havotto.TodoList.Domain.Services
{
    public interface ITodoService
    {
        Task<TodoItem> Create(CreateTodoCommand item);
        Task<TodoItem> MarkCompleted(int id);
        Task<List<TodoItem>> List(ListQuery query);
        Task Delete(int id);
        Task<TodoItem> ChangeTitle(int id, string title);
    }
}
