﻿using Havotto.TodoList.Api.Domain.Repositories;
using Havotto.TodoList.Domain.Commands;
using Havotto.TodoList.Domain.Entities;
using Havotto.TodoList.Domain.Exceptions;
using Havotto.TodoList.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Havotto.TodoList.Domain.Services
{
    public class TodoService : ITodoService
    {
        private readonly ITodoRepository _repo;
        private readonly IUnitOfWork _uow;

        public TodoService(ITodoRepository repo, IUnitOfWork uow)
        {
            this._repo = repo;
            this._uow = uow;
        }

        /// <summary>
        /// Cím változtatás
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public async Task<TodoItem> ChangeTitle(int id, string title)
        {
            var entity = await _repo.FindByIdAsync(id);
            if (entity == null)
                throw new EntityNotFoundException(id, typeof(TodoItem));
            entity.Title = title;
            await _uow.SaveChangesAsync();
            return entity;
        }

        /// <summary>
        /// Létrehozás
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public async Task<TodoItem> Create(CreateTodoCommand cmd)
        {
            var todo = new TodoItem
            {
                Title = cmd.Title,
                IsCompleted = false,
            };
            var createdTodo = _repo.Create(todo);

            await _uow.SaveChangesAsync();

            return createdTodo;
        }

        /// <summary>
        /// Törlés
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task Delete(int id)
        {
            var found = await _repo.Delete(id);
            if (!found)
                throw new EntityNotFoundException(id, typeof(TodoItem));
            await _uow.SaveChangesAsync();
        }

        /// <summary>
        /// Listázás szűrő feltételekkel, opcionálisan lapozással:
        /// -kész/nincs kész
        /// -szövegrészletre keresés
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<List<TodoItem>> List(ListQuery query)
        {
            var filters = BuildFilterList(query);

            return await _repo.List(filters, query.PageIndex);
        }

        public static List<Expression<Func<TodoItem, bool>>> BuildFilterList(ListQuery query)
        {
            var filters = new List<Expression<Func<TodoItem, bool>>>();
            if (query.IsCompleted.HasValue)
                filters.Add(x => x.IsCompleted == query.IsCompleted);

            if (query.TitleContains?.Length > 0)
                filters.Add(x => x.Title.Contains(query.TitleContains));

            return filters;
        }

        public async Task<TodoItem> MarkCompleted(int id)
        {
            var todoItem = await _repo.FindByIdAsync(id);
            if (todoItem is null)
                throw new EntityNotFoundException(id, typeof(TodoItem));

            todoItem.IsCompleted = true;

            await _uow.SaveChangesAsync();

            return todoItem;
        }
    }
}
