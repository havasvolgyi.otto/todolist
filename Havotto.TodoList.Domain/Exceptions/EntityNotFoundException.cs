﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havotto.TodoList.Domain.Exceptions
{
    public class EntityNotFoundException:Exception
    {
        public EntityNotFoundException(int id, Type entityType)
        {
            if (entityType is null)
                throw new ArgumentNullException(nameof(entityType));

            Id = id;
            EntityType = entityType;
        }

        public int Id { get; }
        public Type EntityType { get; }
    }
}
