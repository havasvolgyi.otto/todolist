﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havotto.TodoList.Domain.Queries
{
    public class ListQuery
    {
        /// <summary>
        /// Kész-e a Todo, null esetén nem szűr
        /// </summary>
        public bool? IsCompleted { get; set; }

        /// <summary>
        /// Cím szövegre keresés, null esetén nem keres
        /// </summary>
        public string TitleContains { get; set; }

        /// <summary>
        /// Oldalszám, első oldal indexe 1
        /// </summary>
        public int? PageIndex { get; set; }
    }
}
