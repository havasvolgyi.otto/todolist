﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havotto.TodoList.Domain.Helpers
{
    public static class Conversions
    {
        public static bool? ToNullableBool(this int? n) => n switch
        {
            null => null,
            0 => false,
            _ => true,
        };

    }
}
