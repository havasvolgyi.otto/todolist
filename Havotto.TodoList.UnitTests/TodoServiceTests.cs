﻿using Havotto.TodoList.Api.Domain.Repositories;
using Havotto.TodoList.Domain;
using Havotto.TodoList.Domain.Entities;
using Havotto.TodoList.Domain.Exceptions;
using Havotto.TodoList.Domain.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Havotto.TodoList.UnitTests
{
    public class TodoServiceTests
    {
        [Fact]
        public void ChangeTitleThrowsIfNotFound()
        {
            const int id = 1;
            var uowMock = new Mock<IUnitOfWork>();

            var repoMock = new Mock<ITodoRepository>();
            repoMock.Setup(r => r.FindByIdAsync(id))
                    .Returns(Task.FromResult<TodoItem>(null));

            var sut = new TodoService(repoMock.Object, uowMock.Object);

            Assert.Throws<EntityNotFoundException>(() =>
            {
                sut.ChangeTitle(id, "New").GetAwaiter().GetResult();
            });
            repoMock.Verify(r => r.FindByIdAsync(id), Times.Once);
        }

        [Fact]
        public void DeleteThrowsIfNotFound()
        {
            const int id = 1;
            var uowMock = new Mock<IUnitOfWork>();

            var repoMock = new Mock<ITodoRepository>();
            repoMock.Setup(r => r.Delete(id))
                    .Returns(Task.FromResult(false));

            var sut = new TodoService(repoMock.Object, uowMock.Object);

            Assert.Throws<EntityNotFoundException>(() =>
            {
                sut.Delete(id).GetAwaiter().GetResult();
            });
            repoMock.Verify(r => r.Delete(id), Times.Once);
        }

        [Fact]
        public void MarkCompletedThrowsIfNotFound()
        {
            const int id = 1;

            var uowMock = new Mock<IUnitOfWork>();

            var repoMock = new Mock<ITodoRepository>();
            repoMock.Setup(r => r.FindByIdAsync(id))
                    .Returns(Task.FromResult<TodoItem>(null));

            var sut = new TodoService(repoMock.Object, uowMock.Object);

            Assert.Throws<EntityNotFoundException>(() =>
            {
                sut.MarkCompleted(id).GetAwaiter().GetResult();
            });
            repoMock.Verify(r => r.FindByIdAsync(id), Times.Once);
        }
    }
}
