using Havotto.TodoList.Api.Persistence.Repositories;
using Havotto.TodoList.Domain.Entities;
using Havotto.TodoList.Persistence;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Havotto.TodoList.UnitTests
{
    public class TodoRepositoryTests
    {
        [Fact]
        public async Task DeleteReturnFalseIfNotFound()
        {
            const int id = 1;
            var dbMock = new Mock<TodoDbContext>(new DbContextOptions<TodoDbContext>());
            dbMock.Setup(db => db.FindAsync<TodoItem>(id))
                  .Returns(async () => null);

            var sut = new TodoRepository(dbMock.Object);
            var result = await sut.Delete(id);

            Assert.False(result);
            dbMock.Verify(db => db.FindAsync<TodoItem>(id), Times.Once);
            dbMock.Verify(db => db.Remove(id), Times.Never);
        }

        [Fact]
        public async Task DeleteReturnsTrueIfFound()
        {
            const int id = 1;
            //Arrange
            var entity = new TodoItem { Id = id, Title = "T1" };
            var dbMock = new Mock<TodoDbContext>(new DbContextOptions<TodoDbContext>());
            dbMock.Setup(db => db.FindAsync<TodoItem>(id))
                  .Returns(async () => entity);

            //Act
            var sut = new TodoRepository(dbMock.Object);
            var result = await sut.Delete(entity.Id);

            //Assert
            Assert.True(result);
            dbMock.Verify(db => db.FindAsync<TodoItem>(id), Times.Once);
            dbMock.Verify(db => db.Remove(entity), Times.Once);
        }
    }
}
