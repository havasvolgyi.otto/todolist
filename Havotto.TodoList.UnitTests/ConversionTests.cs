﻿using Havotto.TodoList.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Havotto.TodoList.UnitTests
{
    public class ConversionTests
    {
        [Theory]
        [InlineData(null, null)]
        [InlineData(0, false)]
        [InlineData(1, true)]
        [InlineData(2, true)]
        public void ToNullableBool(int? n, bool? b)
        {
            Assert.Equal(b, Conversions.ToNullableBool(n));
        }
    }
}
