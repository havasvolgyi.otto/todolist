﻿using Havotto.TodoList.Domain.Queries;
using Havotto.TodoList.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Havotto.TodoList.UnitTests
{
    public class BuildFilterListTests
    {
        [Theory]
        [InlineData(null, null, 0)]
        [InlineData(false, null, 1)]
        [InlineData(null, "", 0)]
        [InlineData(null, "keres", 1)]
        [InlineData(true, "mas", 2)]
        public void BuildfilterListCountTest(bool? isCompleted, string titleContains, int listSize)
        {
            var q = new ListQuery()
            {
                IsCompleted = isCompleted,
                TitleContains = titleContains,
            };
            var list = TodoService.BuildFilterList(q);
            Assert.Equal(listSize, list.Count);
        }
    }
}
