﻿using Havotto.TodoList.Api.Dto;
using Havotto.TodoList.Domain.Commands;
using Havotto.TodoList.Domain.Entities;
using Havotto.TodoList.Persistence;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Havotto.TodoList.IntgerationTests
{
    public class TodoTests : IDisposable, IClassFixture<CustomizedWebApplicationFactory<TestStartup>>
    {
        private readonly CustomizedWebApplicationFactory<TestStartup> _factory;
        protected readonly HttpClient _client;

        /// <summary>
        /// Azért kell ez a scope, hogy a tesztek 
        /// scoped objektumai, pl. DbContext újra létrejöjjenek
        /// minden tesztesetben
        /// </summary>
        private IServiceScope _testCaseScope;
        private readonly IServiceProvider _scopedServiceProvider;
        protected readonly TodoDbContext _db;

        public TodoTests(CustomizedWebApplicationFactory<TestStartup> factory,
            ITestOutputHelper output)
        {
            _factory = factory;
            _factory.TestOutput = output;   

            //Arrange common
            _client = _factory.CreateClient();

            _testCaseScope = _factory.Services.CreateScope();
            _scopedServiceProvider = _testCaseScope.ServiceProvider;
            _db = _scopedServiceProvider.GetRequiredService<TodoDbContext>();
            _db.Database.EnsureCreated();
        }

        /// <summary>
        /// Testcase teardown
        /// </summary>
        public void Dispose()
        {
            _db.Database.EnsureDeleted();

            _testCaseScope.Dispose();
        }

        [Fact]
        public async Task UserCanCreateNewTodoItem()
        {
            var cmd = new CreateTodoCommand { Title = "Create" };

            var response = await _client.PostAsJsonAsync("/api/todo", cmd);

            //Assert
            AssertStatusCode200(response);
            var dto = await response.ReadAsJsonAsync<TodoItemDto>();

            Assert.Equal(cmd.Title, dto.Title);

            UnitOfWork(db =>
            {
                Assert.Equal(1, db.Todos.Count());

                var todo = db.Todos.Single();
                Assert.Equal(cmd.Title, todo.Title);
                Assert.False(todo.IsCompleted);
            });
        }

        [Fact]
        public async Task UserCanListTodoItems()
        {
            const int count = 30;
            //Arrange
            CreateTodoItems(count, n => new TodoItem
            {
                Title = $"Title{n}",
                IsCompleted = false
            });

            // Act
            var response = await _client.GetAsync("/api/todo");
            var dto = await response.ReadAsJsonAsync<List<TodoItemDto>>();

            // Assert
            AssertStatusCode200(response);
            Assert.Equal(count, dto.Count);
        }

        [Fact]
        public async Task UserCanMarkTodoCompleted()
        {
            //Arrange
            CreateTodoItems(1, n => new TodoItem
            {
                Title = $"Title{n}",
                IsCompleted = false
            });
            var todoItemBefore = _db.Todos.Single();

            //Act
            var path = $"/api/todo/{todoItemBefore.Id}/mark-completed";
            var response = await _client.PostAsJsonAsync(path);

            //Assert
            AssertStatusCode200(response);
            var dto = await response.ReadAsJsonAsync<TodoItemDto>();

            Assert.Equal(todoItemBefore.Title, dto.Title);
            Assert.True(dto.IsCompleted);


            UnitOfWork(db =>
            {
                Assert.Equal(1, db.Todos.Count());
                var todoItemAfter = db.Todos.Find(todoItemBefore.Id);
                Assert.NotNull(todoItemAfter);
                Assert.Equal(todoItemBefore.Title, todoItemAfter.Title);
                Assert.True(todoItemAfter.IsCompleted);
            });
        }

        [Fact]
        public async Task Http404WhenEntityNotFound()
        {
            //Act
            var path = "/api/todo/1/mark-completed";
            var response = await _client.PostAsJsonAsync(path);

            //Assert
            AssertStatusCode404(response);
        }

        [Fact]
        public async Task UserCanListInCompleteTodoItems()
        {
            const int count = 30;
            //Arrange
            CreateTodoItems(count, n => new TodoItem
            {
                Title = $"Title{n}",
                IsCompleted = n % 2 == 0
            });

            // Act
            var path = "/api/todo?isCompleted=0";
            var response = await _client.GetAsync(path);

            // Assert
            AssertStatusCode200(response);
            var dto = await response.ReadAsJsonAsync<List<TodoItemDto>>();

            Assert.Equal(count / 2, dto.Count);
            Assert.True(dto.All(e => e.IsCompleted == false));
        }

        [Fact]
        public async Task UserCanSearchTodoItemsByText()
        {
            const int count = 30;
            //Arrange
            CreateTodoItems(count, n => new TodoItem
            {
                Title = $"Title{n} keres{n/3}  vege",
                IsCompleted = n % 2 == 0
            });

            // Act
            const string searchFor= "keres2";
            var path = $"/api/todo?titleContains={searchFor}";
            var response = await _client.GetAsync(path);

            // Assert
            AssertStatusCode200(response);
            var dto = await response.ReadAsJsonAsync<List<TodoItemDto>>();

            Assert.Equal(3, dto.Count);
            Assert.True(dto.All(e => e.Title.Contains(searchFor)));
        }

        [Fact]
        public async Task UserCanPaginateTodoList()
        {
            const int count = 30;
            //Arrange
            CreateTodoItems(count, n => new TodoItem
            {
                Title = $"Title{n}",
                IsCompleted = n % 2 == 0,
            });

            // Act
            //második oldal: 26-30. elemek
            var path = $"/api/todo?pageIndex=2";
            var response = await _client.GetAsync(path);

            // Assert
            AssertStatusCode200(response);
            var dto = await response.ReadAsJsonAsync<List<TodoItemDto>>();

            Assert.Equal(5, dto.Count);
        }


        [Fact]
        public async Task UserCanDeleteTodo()
        {
            //Arrange
            CreateTodoItems(2, n => new TodoItem
            {
                Title = $"Title{n}",
                IsCompleted = false
            });
            var todoItemToDelete = _db.Todos.First();

            //Act
            var path = $"/api/todo/{todoItemToDelete.Id}";
            var response = await _client.DeleteAsync(path);

            //Assert
            AssertStatusCode204(response);

            UnitOfWork(db =>
            {
                Assert.Equal(1, db.Todos.Count());
                var todoItemAfter = db.Todos.Find(todoItemToDelete.Id);
                Assert.Null(todoItemAfter);
            });
        }

        [Fact]
        public async Task UserCanChangeTitle()
        {
            //Arrange
            CreateTodoItems(1, n => new TodoItem
            {
                Title = $"Title{n}",
                IsCompleted = false
            });
            var todoItemBefore = _db.Todos.Single();

            var cmd = new ChangeTodoTitleCommand { Title = "NewTitle" };

            //Act
            var path = $"/api/todo/{todoItemBefore.Id}/change-title";
            var response = await _client.PostAsJsonAsync(path, cmd);

            //Assert
            AssertStatusCode200(response);
            var dto = await response.ReadAsJsonAsync<TodoItemDto>();

            Assert.Equal(cmd.Title, dto.Title);
            Assert.Equal(todoItemBefore.IsCompleted, dto.IsCompleted);


            UnitOfWork(db =>
            {
                Assert.Equal(1, db.Todos.Count());
                var todoItemAfter = db.Todos.Find(todoItemBefore.Id);
                Assert.NotNull(todoItemAfter);
                Assert.Equal(cmd.Title, todoItemAfter.Title);
                Assert.Equal(todoItemBefore.IsCompleted, todoItemAfter.IsCompleted);
            });
        }


        #region Helpers

        private void AssertStatusCode200(HttpResponseMessage res)
        {
            Assert.Equal(System.Net.HttpStatusCode.OK, res.StatusCode);
        }

        private void AssertStatusCode204(HttpResponseMessage res)
        {
            Assert.Equal(System.Net.HttpStatusCode.NoContent, res.StatusCode);
        }

        private void AssertStatusCode404(HttpResponseMessage res)
        {
            Assert.Equal(System.Net.HttpStatusCode.NotFound, res.StatusCode);
        }

        /// <summary>
        /// Létrehoz megadott db todo-t az adatbázisban
        /// </summary>
        /// <param name="count"></param>
        /// <param name="entityFactory"></param>
        private void CreateTodoItems(int count, Func<int, TodoItem> entityFactory)
        {
            UnitOfWork(db =>
            {
                for (int i = 0; i < count; i++)
                {
                    var entity = entityFactory(i);
                    db.Add(entity);
                    db.SaveChanges();
                }
            });
        }

        /// <summary>
        /// Azért van szükség erre, hogy külön dbcontext 
        /// lehessen a teszteset elején és végén,
        /// különban zavarják egymást a cache-elés miatt
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        private void UnitOfWork(Action<TodoDbContext> action)
        {
            using (var scope = _scopedServiceProvider.CreateScope())
            {
                var scopedServiceProvider = scope.ServiceProvider;
                var db = scopedServiceProvider.GetRequiredService<TodoDbContext>();
                action(db);
            }
        }
        #endregion
    }
}
