﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Havotto.TodoList.IntgerationTests
{
    /// <summary>
    /// Segésfüggvények a HTTP kérésekhez
    /// </summary>
    public static class HttpClientExtensions
    {
        public static Task<HttpResponseMessage> PostAsJsonAsync<T>(
            this HttpClient httpClient, string path, T data)
        {
            var dataAsString = JsonConvert.SerializeObject(data);
            var content = new StringContent(dataAsString);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return httpClient.PostAsync(path, content);
        }

        public static Task<HttpResponseMessage> PostAsJsonAsync(
            this HttpClient httpClient, string path)
        {
            var content = new StringContent("0");
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return httpClient.PostAsync(path, content);
        }

        public static async Task<T> ReadAsJsonAsync<T>(this HttpResponseMessage message)
        {
            var dataAsString = await message.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(dataAsString);
        }
    }
}
