﻿using Havotto.TodoList.Api.Persistence.Repositories;
using Havotto.TodoList.Domain.Commands;
using Havotto.TodoList.Domain.Entities;
using Havotto.TodoList.Domain.Exceptions;
using Havotto.TodoList.Domain.Queries;
using Havotto.TodoList.Domain.Services;
using Havotto.TodoList.Persistence;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Havotto.TodoList.IntgerationTests.ServiceLayerTests
{
    /// <summary>
    /// Integrációs tesztek a következőkkel:
    /// -TodoService
    /// -TodoRepository
    /// -TodoDbContext
    /// -InMemory Sqlite
    /// </summary>
    public class TodoServiceLayerTests : IDisposable
    {
        private const string ConnectionString = "DataSource=:memory:";
        private readonly SqliteConnection _connection;
        private Func<TodoDbContext> _dbContextFactory;
        private TodoService _todoService;

        public TodoServiceLayerTests()
        {
            _connection = new SqliteConnection(ConnectionString);
            _connection.Open();
            var options = new DbContextOptionsBuilder<TodoDbContext>()
                .UseSqlite(_connection).Options;
            _dbContextFactory = () => new TodoDbContext(options);

            //Arrange objects

            var todoDbContext = _dbContextFactory();
            todoDbContext.Database.EnsureCreated();

            var repo = new TodoRepository(todoDbContext);
            var uow = new EfCoreUnitOfWork(todoDbContext);
            _todoService = new TodoService(repo, uow);

        }

        public void Dispose()
        {
            _connection?.Dispose();
        }

        [Fact]
        public async Task UserCanQueryEmptyTodoList()
        {
            var items = await _todoService.List(new ListQuery());

            items.ShouldBeEmpty();
        }

        [Fact]
        public async Task UserCanQueryAllTodos()
        {
            CreateTodoItems(30, n => new TodoItem
            {
                Title = $"Title{n}",
                IsCompleted = (n % 2 == 0),
            });

            //Act
            var items = await _todoService.List(new ListQuery());

            items.Count.ShouldBe(30);
        }

        [Fact]
        public async Task UserCanFilterTodos()
        {
            CreateTodoItems(30, n => new TodoItem
            {
                Title = $"Title{n} {(n % 3 == 0 ? "keres" : "")}",
                IsCompleted = (n % 2 == 0),
            });

            var query = new ListQuery
            {
                IsCompleted = false,
                TitleContains = "keres",
            };

            //Act
            var items = await _todoService.List(query);

            items.Count.ShouldBe(30 / 2 / 3);
            items.ShouldAllBe(item => item.IsCompleted == query.IsCompleted);
            items.ShouldAllBe(item => item.Title.Contains(query.TitleContains));
        }

        [Fact]
        public async Task UserCanCreateNewTodoItem()
        {
            var cmd = new CreateTodoCommand
            {
                Title = "Elso",
            };

            var createdItem = await _todoService.Create(cmd);

            createdItem.Title.ShouldBe(cmd.Title);
            createdItem.IsCompleted.ShouldBeFalse();

            UnitOfWork(db =>
            {
                db.Todos.ShouldHaveSingleItem();
                var item = db.Todos.Single();
                item.Title.ShouldBe(cmd.Title);
                item.IsCompleted.ShouldBeFalse();
            });
        }

        [Fact]
        public async Task ChangeTitleThrowsNotFoundExceptionWhenNotFound()
        {
            await Assert.ThrowsAsync<EntityNotFoundException>(async () =>
            {
                var updatedItem = await _todoService.ChangeTitle(2, "new title");
            });
        }

        [Fact]
        public async Task DeleteThrowsNotFoundExceptionWhenNotFound()
        {
            await Assert.ThrowsAsync<EntityNotFoundException>(async () =>
            {
                await _todoService.Delete(2);
            });
        }

        [Fact]
        public async Task MArkCompletedThrowsNotFoundExceptionWhenNotFound()
        {
            await Assert.ThrowsAsync<EntityNotFoundException>(async () =>
            {
                await _todoService.MarkCompleted(2);
            });
        }


        #region Helpers

        /// <summary>
        /// Létrehoz megadott db todo-t az adatbázisban
        /// </summary>
        /// <param name="count"></param>
        /// <param name="entityFactory"></param>
        private void CreateTodoItems(int count, Func<int, TodoItem> entityFactory)
        {
            UnitOfWork(db =>
            {
                for (int i = 0; i < count; i++)
                {
                    var entity = entityFactory(i);
                    db.Add(entity);
                    db.SaveChanges();
                }
            });
        }

        private void UnitOfWork(Action<TodoDbContext> action)
        {
            using var db = _dbContextFactory();
            action(db);
        }

        #endregion
    }
}
