﻿using Havotto.TodoList.Api;
using Havotto.TodoList.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Havotto.TodoList.IntgerationTests
{
    public class TestStartup : Startup
    {
        public TestStartup(IConfiguration configuration)
            : base(configuration)
        {
        }

        protected override void AddDbContext(IServiceCollection services)
        {
            //teszteléskor Sqlite-ot használunk
            services.AddDbContext<TodoDbContext>(options =>
            {
                options.UseSqlite("DataSource=testtodo.db.");
            });
        }
    }
}
