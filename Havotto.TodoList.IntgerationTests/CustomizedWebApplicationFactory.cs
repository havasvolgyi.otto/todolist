﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace Havotto.TodoList.IntgerationTests
{
    public class CustomizedWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup>
        where TStartup : class
    {
        public ITestOutputHelper TestOutput { get; set; }
        protected override IHostBuilder CreateHostBuilder()
        {
            var builder = Host.CreateDefaultBuilder()
                .ConfigureWebHostDefaults(x =>
                {
                    x.UseStartup<TestStartup>()
                     .UseTestServer();
                })
                .ConfigureLogging(b =>
                {
                    b.ClearProviders();
                    b.AddXUnit(TestOutput);
                });
            return builder;
        }
    }
}
